
require_relative 'inventariable'

module Item

  class Item
    include Inventariable

    attr_accessor :attributes

    def initialize(attributes)
      @attributes = attributes
    end
  end

  class Shirt < Item
  end

  class Pant < Item
  end

  class Accessory < Item
  end

end

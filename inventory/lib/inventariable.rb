
module Inventariable

  def self.included(klass)
    klass.extend ClassMethods
    klass.extend Enumerable
  end

  module ClassMethods
    def create(attributes)
      new(attributes).tap { |object| instances.push object }
    end

    def instances
      @intances ||= []
    end

    def each(&block)
      instances.each(&block)
    end

    def report(title, items)
      puts "-" * 50
      puts title
      puts "-" * 50

      items.each do |item|
        line = []
        line.push "Item: #{item.attributes[:name]}"
        line.push "Stock: #{item.stock_count}"
        if item.attributes.include? :size
          line.push "Size: #{item.attributes[:size]}"
        end
        puts line.join("\t")
      end

      puts "-" * 50
      puts "\n"
    end

    def in_stock_report
      title = "#{self.to_s} In Stock Report"
      reportable = instances.select { |instance| instance.in_stock? }
      report title, reportable
    end

    def out_of_stock_report
      title = "#{self.to_s} In Stock Report"
      reportable = instances.select { |instance| !instance.in_stock? }
      report title, reportable
    end
  end

  def stock_count
    @stock_count ||= 0
  end

  def stock_count=(number)
    @stock_count = number
  end

  def in_stock?
    stock_count > 0
  end

end

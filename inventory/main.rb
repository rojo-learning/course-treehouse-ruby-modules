
require_relative 'lib/items'

shirt = Item::Shirt.create(name: "MTF", size: "L")
shirt.stock_count = 10

shirt = Item::Shirt.create(name: "MTF2", size: "L")

shirt = Item::Shirt.create(name: "MTF", size: "M")
shirt.stock_count = 9

pant = Item::Pant.create(name: "Jeans", size: "M")
pant.stock_count = 2

pant = Item::Pant.create(name: "Jeans", size: "S")
pant.stock_count = 4

accessory = Item::Accessory.create(name: "Belt", size: "M")
accessory.stock_count = 1

accessory = Item::Accessory.create(name: "Belt", size: "L")
accessory.stock_count = 1

accessory = Item::Accessory.create(name: "Necklace")
accessory.stock_count = 1

Item::Shirt.in_stock_report
Item::Pant.in_stock_report
Item::Accessory.in_stock_report

Item::Shirt.out_of_stock_report


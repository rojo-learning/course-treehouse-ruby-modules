
# Treehouse - Address Book #

This repository contains notes and practice examples from the course
**Ruby Modules**, imparted by Jason Seifer at [Threehouse][JF].

> Modules are an extremely powerful utility when coding in Ruby. Modules allow
> you to add behavior to classes, hold constants, add name spaces, and more.

## Contents ##

- **Ruby Module Basics**: In this stage, is to introduce the basics of working
  with modules in Ruby as well as give the students brief exposure to more
  advanced topics with modules.
- **Ruby Code Modules**: In this stage, we'll learn about some of the modules
  that are included as part of the Ruby core distribution.
- **Include and Extend**: Ruby Modules give you two different ways to augment
  behavior: Include and Extend. In this stage, we'll learn about the differences
  between the two as well as how to use them when creating our own modules.
- **Store Inventory Using Modules**: Now that we now how modules work, we’re
  going to write a simple program that simulates keeping inventory at a small
  retail store. The store sells shirts, pants, and accessories. We’re going to
  put everything together using modules, include, extend, and friends.


---
This repository contains code examples from Treehouse. These are included under
fair use for showcasing purposes only. Those examples may have been modified to
fit my particular coding style.

[JF]: http://teamtreehouse.com


=begin

### Ruby Modules ###

Used as containers for behaviors and occasionally for storage, Modules are alike
to classes that can't be instanced. The most common use is to mix into classes
to provide shared behaviors.

module = SimpleModule
  VERSION = 1.0
end

SimpleModule::VERSION # => 1.0

# Namespaces

A namespace can be thought of as a container for other items. These things can
be classes, constants, other modules, and more. Using namespaces can help to
avoid potential name clashes.

module LaserBots
  module Console
    class Command
    end
  end

  module World
    class Player
      attr_reader :name
      def initialize(name)
        @name = name
      end
    end
  end
end

player = LaserBots::World::Player.new('David')
player.name # => 'David'


# The Comparable Module

This module allows to make instances of a class sortable when a spaceship `<=>`
is defined within them.

class Player
  include Comparable

  attr_accessor :name, :score

  def <=>(other_player)
    score <=> other_player.score
  end

  def initialize(name, score)
    @name  = name
    @score = score
  end
end

player1 = Player.new('David', 100)
player2 = Player.new('Salvador', 80)

player1 > player2 # => true
player1 < player2 # => false


# The Math Module

This module provides mathematical functions and constants.

Math::E          # => 2.718...
Math::Pi         # => 4.141...
Math.sqrt 9      # => 3.0
Math.cos 1       # => 0.540...
Math.hypot 2, 2  # => 2.828...
Math.log 2, 10   # => 0.301...

# The Enumerable Module

This module provides functions quite useful to operate over collections. For
this, a class must define an `each` method. Also, a spaceship `<=>` method
should be defined for functions that require ordering.

class Game
  include Enumerable

  attr_accessor :players

  def initialize
    @players = []
  end

  def each(&block)
    players.each(&block)
  end

  def add_player(player)
    players.push player
  end

  def score
    score = 0
    players.each do { |player| score += player.score }
    score
  end
end

game = Game.new
game.add_player(player1)
game.add_player(player2)

game.each { |player| puts player.score }
game.any? { |player| player.score > 80 }
game.select { |player| player.score > 70 }
game.find { |player| player.score < 90 }

# Include

The `include` function allows to include the methods and constants of a module
into a class, being available at an instance level.

module Fetcher
  def fetch(item)
    puts "I'll bring that #{item} right back!"
  end
end

class Dog
  include Fetcher

  attr_reader :name

  def initialize(name)
    @name = name
  end
end

dog = Dog.new('Fido')
dog.fetch 'ball'

A module can have defined specific behavior that is triggered when included.
This is declared inside the `self.included` method definition. It can be useful
to run methods, write methods, or include other modules.

module Fetcher
  def self.include(klass)
    attr_accessor :fetch_count
  end

  def fetch(item)
    @fetch_count ||= 0
    @fetch_count  += 1
    puts "[#{@name}, Fetch count: #{@fetch_count}] I'll bring that #{item} right back!"
  end
end

class Dog
  include Fetcher

  attr_reader :name

  def initialize(name)
    @name = name
  end
end

dog = Dog.new('Fido')
dog.fetch 'ball'
dog.fetch 'toy'

# Extend

Unlike `include`, `extend` allows to add its functions and constants at the
class level.

module Tracking
  def create(name)
    object = new(name)
    instances.push(object)
    return object
  end

  def instances
    @instances ||= []
  end

  def find(name)
    instances.find { |instance| instance.name == name }
  end
end

class Customer
  extend Tracking

  attr_accessor :name

  def initialize(name)
    @name = name
  end

  def to_s
    "[#{@name}]"
  end
end

Customer.instances # => []
Customer.create 'David' # => <Customer: @name='David'>
Customer.create 'Juan' # => <Customer: @name='Juan'>
Customer.instances # => [<Customer: @name='David'>, <Customer: @name='Juan'>]
Customer.find 'Juan' # => <Customer: @name='Juan'>

=end
